<?php
class Users {
    private $model;

    // ce controlleur a besoin du modèle (de l'accès à la DB) pour fonctionner.
    // cette méthode sera appelée par index.php afin de le recevoir automatiquement.
    public function loadModel($model) {
        $this->model=$model;
    }

    public function list() {
        $users=$this->model->listUsers(); 
        require "../app/view/_templates/header.php";
        require "../app/view/_templates/menu.php";
        require "../app/view/users.php";
        require "../app/view/_templates/footer.php";
    }

    public function show() {
        $user=$this->model->loadUser($_REQUEST["login"]); 
        require "../app/view/_templates/header.php";
        require "../app/view/_templates/menu.php";
        require "../app/view/showUser.php";
        require "../app/view/_templates/footer.php";
    }

    public function delete() {
        // est-ce un appel Ajax de type POST ?
        if ($_SERVER["REQUEST_METHOD"]=="POST") { // oui
            $login=$_POST["login"];
            if ($login!=$_SESSION["login"]) {
                if (!$this->model->deleteUser($login)) {
                    http_response_code(422); // 422=Unprocessable Entity
                    echo "Erreur lors de l'effacement";
                }
            } else {
                http_response_code(422);
                echo "Impossible de s'effacer soi-même.";
            }
        } else {
            http_response_code(404); // 404=Not Found
            require "../app/view/_templates/error.php";    
        }
    }
}