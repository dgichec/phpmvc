<?php
/* Cette vue est un fragment d'HTML gère le haut du document :
*    - Affiche une banière avec un logo, titre et les boutons gérant la connexion.
*    - Insère les feuilles de style.
*/
    global $css, $title;
    array_unshift($css, "css/style.css"); // place toujours cette feuille en tête
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $title; ?></title>
        <?php // insère toutes les feuilles de style
            foreach ($css as $f) {
                echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"$f\">\n";
            } 
        ?>
    </head>
<body>
    <div class="topbar">
        <img src="img/logo.png">
        <a href="index.php" class="logolink"><?php echo $title; ?></a>
        <span>
        <?php 
        if (isset($_SESSION["name"])) { // suivant qu'on est loggé ou pas
            // on peut se délogger
            echo $_SESSION["name"] . '&#x263A; <a href="index.php?c=auth&a=logout">Se délogger</a>';
        } else {
            // on peut s'inscrire ou se logger
            echo '<a href="#" id="login">S\'authentifier</a><a href="index.php?c=auth&a=subscribe">S\'inscrire</a>';
        }
        ?>
        </span>
    </div>
    <div class="main">
    
