<?php
/* Cette vue est un fragment d'HTML qui gère une fenêtre popup :
*     - Cette fenêtre permettra à l'utilisateur de s'authentifier.
*     - C'est auth.js qui gère cette fenêtre, via Javascript.
*/
global $js;
array_push($js,"js/auth.js");
?>
<div class="loginbox">
    <div class="hider"></div>
    <form>
        <div class="loginboxtop">
            <div>
                <label for="inputlogin">Authentifiant</label> : <input type="text" name="login" id="inputlogin">
            </div>
            <div>
                <label for="inputpassword">Mdp</label> : <input type="password" name="password" id="inputpassword">
            </div>
            <div class="error">
            </div>
        </div>
        <hr>
        <input type="submit" value="Annuler" class="roundedButton">
        <input type="submit" value="Ok" class="roundedButton">
    </form>
</div>
