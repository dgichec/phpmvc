<?php
/* Cette vue est un fragment d'HTML qui affiche le menu sur la gauche.
*/
global $js; 
array_push($js,"js/menu.js"); // ce fichier js se chargera de faire ressortir le menu actif en fonction de l'URL de la page
?> 
<div class="menu">
    <ul>
        <li><a href="index.php">Accueil</a></li>
        <li><a href="index.php?c=profile&a=edit">Mon profil</a></li>
        <li><a href="index.php?c=users&a=list">Liste des utilisateurs</a></li>
        <li class="hidden"><a href="index.php?c=users&a=show">Info utilisateur</a></li>
    </ul>
</div>

