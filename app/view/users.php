<div class="center">
    <table>
        <thead>
            <tr>
                <th>id</th><th>login</th><th>name</th>
            </tr>
        </thead>
        <tbody>
                <?php foreach ($users as $user) { ?>
                <tr>
                    <td><a href="index.php?c=users&a=show&login=<?php if (isset($user["login"])) echo htmlspecialchars($user["login"], ENT_QUOTES, 'UTF-8'); ?>"><?php if (isset($user["id"])) echo htmlspecialchars($user["id"], ENT_QUOTES, 'UTF-8'); ?></a></td>
                    <td><?php if (isset($user["login"])) echo htmlspecialchars($user["login"], ENT_QUOTES, 'UTF-8'); ?></td>
                    <td><?php if (isset($user["name"])) echo htmlspecialchars($user["name"], ENT_QUOTES, 'UTF-8'); ?></td>
                    <td><button onclick="deleteUser('<?php if (isset($user["login"])) echo htmlspecialchars($user["login"], ENT_QUOTES, 'UTF-8'); ?>')">X</button></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<?php
global $js;
array_push($js,"js/users.js");
?>